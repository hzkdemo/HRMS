/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50612
Source Host           : 127.0.0.1:3306
Source Database       : hrms

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2015-04-27 20:46:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for think_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `think_auth_group`;
CREATE TABLE `think_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_auth_group
-- ----------------------------
INSERT INTO `think_auth_group` VALUES ('1', '普通用户', '1', '1,2,6,7,13,16,23,24,32,34,35');
INSERT INTO `think_auth_group` VALUES ('2', '网站编辑', '1', '1,2,3,4,6,7,13,16,23,24,29,30,32,34,35');
INSERT INTO `think_auth_group` VALUES ('3', '管理员', '1', '1,2,3,4,5,6,7,8,9,10,11,13,16,20,23,24,29,30,32,33,34,35');
INSERT INTO `think_auth_group` VALUES ('4', '超级管理员', '1', '');

-- ----------------------------
-- Table structure for think_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `think_auth_group_access`;
CREATE TABLE `think_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_auth_group_access
-- ----------------------------
INSERT INTO `think_auth_group_access` VALUES ('1', '4');
INSERT INTO `think_auth_group_access` VALUES ('2', '3');
INSERT INTO `think_auth_group_access` VALUES ('3', '2');
INSERT INTO `think_auth_group_access` VALUES ('4', '1');
INSERT INTO `think_auth_group_access` VALUES ('5', '2');

-- ----------------------------
-- Table structure for think_auth_module
-- ----------------------------
DROP TABLE IF EXISTS `think_auth_module`;
CREATE TABLE `think_auth_module` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_auth_module
-- ----------------------------
INSERT INTO `think_auth_module` VALUES ('1', '通告管理');
INSERT INTO `think_auth_module` VALUES ('2', '用户管理');
INSERT INTO `think_auth_module` VALUES ('3', '权限管理');
INSERT INTO `think_auth_module` VALUES ('4', '其他');

-- ----------------------------
-- Table structure for think_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `think_auth_rule`;
CREATE TABLE `think_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  `module_id` tinyint(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_auth_rule
-- ----------------------------
INSERT INTO `think_auth_rule` VALUES ('1', 'Home/Index/index', '访问首页', '1', '1', '', '4');
INSERT INTO `think_auth_rule` VALUES ('2', 'Home/Message/index', '通告首页', '1', '1', '', '1');
INSERT INTO `think_auth_rule` VALUES ('3', 'Home/Message/add', '发布通告', '1', '1', '', '1');
INSERT INTO `think_auth_rule` VALUES ('4', 'Home/Message/edit', '编辑通告', '1', '1', '', '1');
INSERT INTO `think_auth_rule` VALUES ('5', 'Home/Message/del', '删除通告', '1', '1', '', '1');
INSERT INTO `think_auth_rule` VALUES ('6', 'Home/Message/cate', '通告内容', '1', '1', '', '1');
INSERT INTO `think_auth_rule` VALUES ('7', 'Home/User/index', '用户列表', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('8', 'Home/User/add', '添加用户', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('9', 'Home/User/addHandle', '添加用户操作', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('10', 'Home/User/edit', '编辑用户', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('11', 'Home/User/editHandle', '编辑用户操作', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('12', 'Home/User/lock', '锁定用户', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('13', 'Home/Position/index', '职位列表', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('14', 'Home/Position/add', '添加职位', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('15', 'Home/Position/delete', '删除职位', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('16', 'Home/Group/index', '用户组列表', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('17', 'Home/Group/add', '添加用户组', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('18', 'Home/Group/edit', '编辑用户组', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('19', 'Home/Group/addAuth', '用户组权限', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('20', 'Home/Auth/index', '权限列表', '1', '1', '', '3');
INSERT INTO `think_auth_rule` VALUES ('21', 'Home/Auth/add', '添加权限', '1', '1', '', '3');
INSERT INTO `think_auth_rule` VALUES ('22', 'Home/Auth/del', '删除权限', '1', '1', '', '3');
INSERT INTO `think_auth_rule` VALUES ('23', 'Home/System/index', '系统信息', '1', '1', '', '4');
INSERT INTO `think_auth_rule` VALUES ('24', 'Home/System/welcome', '欢迎界面', '1', '1', '', '4');
INSERT INTO `think_auth_rule` VALUES ('25', 'Home/Auth/addHandle', '添加权限操作', '1', '1', '', '3');
INSERT INTO `think_auth_rule` VALUES ('26', 'Home/Group/addHandle', '添加用户组操作', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('27', 'Home/Group/editHandle', '编辑用户组操作', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('28', 'Home/Group/authHandle', '编辑权限操作', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('29', 'Home/Message/addHandle', '发布通告操作', '1', '1', '', '1');
INSERT INTO `think_auth_rule` VALUES ('30', 'Home/Message/editHandle', '编辑通告操作', '1', '1', '', '1');
INSERT INTO `think_auth_rule` VALUES ('31', 'Home/Position/addHandle', '添加职位操作', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('32', 'Home/User/content', '用户信息', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('33', 'Home/User/uploadImg', '上传照片', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('34', 'Home/User/pass', '修改密码', '1', '1', '', '2');
INSERT INTO `think_auth_rule` VALUES ('35', 'Home/User/passHandle', '修改密码操作', '1', '1', '', '2');

-- ----------------------------
-- Table structure for think_message
-- ----------------------------
DROP TABLE IF EXISTS `think_message`;
CREATE TABLE `think_message` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `content` varchar(255) NOT NULL,
  `user_id` int(8) unsigned NOT NULL,
  `release_time` varchar(20) NOT NULL,
  `trash` int(8) unsigned zerofill NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_message
-- ----------------------------
INSERT INTO `think_message` VALUES ('1', '测试', '&lt;h1&gt;\r\n	这仅仅是一个测试啊！\r\n&lt;/h1&gt;', '1', '1429190634', '00000000');
INSERT INTO `think_message` VALUES ('2', '测试12', '这好似一个测试！11112222', '1', '1429536932', '00000000');

-- ----------------------------
-- Table structure for think_position
-- ----------------------------
DROP TABLE IF EXISTS `think_position`;
CREATE TABLE `think_position` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_position
-- ----------------------------
INSERT INTO `think_position` VALUES ('1', '员工');
INSERT INTO `think_position` VALUES ('2', '组长');
INSERT INTO `think_position` VALUES ('3', '经理');
INSERT INTO `think_position` VALUES ('4', '总经理');

-- ----------------------------
-- Table structure for think_user
-- ----------------------------
DROP TABLE IF EXISTS `think_user`;
CREATE TABLE `think_user` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `emp_id` int(10) unsigned NOT NULL,
  `position_id` int(8) unsigned zerofill DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(100) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `img` varchar(50) NOT NULL,
  `login_time` varchar(30) NOT NULL,
  `status` int(1) unsigned zerofill NOT NULL,
  `age` int(5) unsigned NOT NULL,
  `sex` int(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_user
-- ----------------------------
INSERT INTO `think_user` VALUES ('1', '10010', '00000004', 'admin', 'a0d65373e82aed621cb9f8efd327da02', '313707304@qq.com', '13283885629', '', '1429186731', '0', '25', '0');
INSERT INTO `think_user` VALUES ('2', '10012', '00000003', '张三', 'a0d65373e82aed621cb9f8efd327da02', '313@qq.com', '13211111111', '/upload/image/201504/552fb8bf3adcc.jpg', '1429190850', '0', '30', '0');
INSERT INTO `think_user` VALUES ('3', '10013', '00000002', '李四', 'a0d65373e82aed621cb9f8efd327da02', '323@qq.com', '133333333333333', '/upload/image/201504/552fb8f97f061.jpg', '1429190908', '0', '28', '0');
INSERT INTO `think_user` VALUES ('4', '10014', '00000001', '王五', 'a0d65373e82aed621cb9f8efd327da02', '333@qq.com', '44444444', '/upload/image/201504/552fb92b749d9.jpg', '1429190957', '0', '30', '0');
INSERT INTO `think_user` VALUES ('5', '10015', '00000003', '马六', 'a0d65373e82aed621cb9f8efd327da02', '353@qq.com', '134555', '/upload/image/201504/553504e2298c1.jpg', '1429538051', '0', '60', '0');
