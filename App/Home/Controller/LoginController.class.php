<?php
/**
 * Created by PhpStorm.
 * User: Shen
 * Date: 2015/3/24
 * Time: 20:43
 */
namespace Home\Controller;
use Think\Controller;
class LoginController extends  Controller{

//    登陆控制器
    public function index(){

        $this->display();

    }
//    登陆操作
    public function login(){
         // var_dump(I());
         if(!IS_POST)$this->error("异常请求！");
         $user = M('user')->where('emp_id=%d',I('post.emp_id'))->find();
         if ($user['password'] == md5(I('password'))) {
            if ($user['status'] == 0) {
                 session('uid',$user['id']);
                 session('username',$user['username']);
                 $this->success("登陆成功！",__APP__."/Home");
            }else{
                $this->error("该用户已被锁定，请联系管理员！");
            }
         }else{
            $this->error("员工号或密码错误！请重新登陆！");
         }
    }

//    登出操作
    public function logout(){
            session(null);
            session('username',null);
            session('uid',null);
           redirect(__MODULE__.'/Login/');

    }

}