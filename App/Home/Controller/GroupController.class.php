<?php
namespace Home\Controller;
use Think\Controller;
class GroupController extends CommonController {
    public function index(){
	//获取用户组
		$group = M('auth_group');
        $count = $group->count();
        $Page  = new \Library\Page($count,8);// $number为分页数
        $Page->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% <li>%HEADER%</li>');
        $data['page'] = $Page->show();// 分页显示输出
        $data['groups'] =$group->order('id asc')
                ->limit($Page->firstRow.','.$Page->listRows)
                ->select();
    	
    	$this->page = $data['page'];
    	$this->groups = $data['groups'];
        $this->display();
    }

    // 添加用户组
    public function add(){
    	$this->display();
    }

    // 添加用户组操作
    public function addHandle(){
    	if(!IS_POST)$this->error('异常请求！');
    	// var_dump(I());die;
    	$group = M('auth_group');
    	if(I('title') == '')$this->error("用户组名不能为空！");
    	$rules = array(
            array('title','','添加失败：用户组名称重复！',0,'unique',3), 
        );
        $group->setProperty('_validate',$rules);
    	if (!$group->create()) {
    		$this->error($message->getError());
    	}else{
    		if ($group->add(I())) {
    			$this->success("添加成功！",__MODULE__."/Group/");
    		}else{
    			$this->error("添加失败！");
    		}
    	}

    } 

    //编辑用户组
    public function edit(){
    	$group = M('auth_group')->where('id=%d',I('id'))->find();
    	$this->group = $group;
    	// var_dump($group);die;
    	$this->display();
    }

    //编辑用户组操作
    public function editHandle(){
    	if(!IS_POST)$this->error('异常请求！');
    	// var_dump(I());die;
    	$group = M('auth_group');
    	if(I('title') == '')$this->error("用户组名不能为空！");
    	$rules = array(
            array('title','','修改失败：用户组名称重复！',0,'unique',3), 
        );
        $group->setProperty('_validate',$rules);
        $data['id'] = I('id');
        $data['title'] = I('title');
        $data['status'] = I('status');
		if ($group->save($data)) {
			$this->success("修改成功！",__MODULE__."/Group/");
		}else{
			$this->error("修改失败！");
		}
    }


    // 添加权限
    public function addAuth(){
        $rule = D('AuthRule')->order('id asc')->select();
        $this->rule=$rule;

        $group = M('auth_group')->where('id=%d',I('get.id'))->find();
        $this->group=$group['title'];
        $this->id=I('id');
        $this->display('auth');
    }

    // 添加权限操作
    public function authHandle(){
        if(!IS_POST)$this->error('异常请求！');
        // var_dump(I('rule'));
        $group = M('auth_group');
        $rule = I('rule');
        $data['rules'] = implode(",", $rule);
        // var_dump($data['rules']);die;
        $data['id'] = I('id');
        if($group->save($data)){
                $this->success("添加成功！",__MODULE__."/Group/");
        }else{
                $this->error("添加失败！");
        }
    }

}
    