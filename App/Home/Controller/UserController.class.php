<?php
/**
 * Created by PhpStorm.
 * User: Shen
 * Date: 2015/3/31
 * Time: 21:57
 */
namespace Home\Controller;
use Think\Controller;
class UserController extends CommonController {
    public function index(){
        $data = D("UserView")->getUsers(8,'id asc');
       	// var_dump($data);die;
        $this->users = $data['users']; 
        $this->page    = $data['page'];   
        $this->assign("title","用户列表");
        $this->display();
    }

    // 添加用户
    public function add(){
        $position = M("position")->select();

        $uid = session('uid');
        $access = M('auth_group_access')->where('uid=%d',$uid)->find();
        if ($access['group_id'] == 4) {
            $group = M('auth_group')->select(); 
        }else{
            $group = M('auth_group')->where('id<>4')->select();
        }

        $this->position = $position;
        $this->group = $group;
        $this->display();
    }

    //  添加用户操作
    public function addHandle(){
        // var_dump(I());die;
        if (!IS_POST)$this->error('异常请求');
        $user = D("user");
        
        if (!$user->create()) {
             $this->error($user->getError());
        }else{
                if ($uid=$user->add()) {
                    if (M('auth_group_access')->add(array('uid'=>$uid,'group_id'=>I('group_id')))) {
                        $this->success("添加成功",__MODULE__."/User/");
                    }else{
                        $this->error("添加失败");
                    }
                }else{
                    $this->error("添加失败");
                }
        }
    }

    // 用户信息
    public function content(){
        
        $group = M('auth_group')->select();
        $this->group = $group;
        $uid = I('id');
        $user = D("user")->where('id=%d',$uid)->find();
        // var_dump($user);die;
        $group = M('auth_group_access')->where('uid=%d',$uid)->find();
        // var_dump($group);die;
        $this->id = $user['id'];
        $this->emp_id = $user['emp_id'];
        $this->username=$user['username'];
        $this->email = $user['email'];
        $this->tel = $user['tel'];
        $this->img = __ROOT__.$user['img'];
        $this->age = $user['age'];
        $this->group_id = $group['group_id'];
       
        $position = M("position")->where('id=%d',$user['position_id'])->find();
        $this->position_name = $position['name'];

        if ($user['sex'] == 0) {
           $this->sex = "男";
        }else{
            $this->sex = "女";
        }
        $this->display();
    }

    // 编辑
    public function edit(){
        $position = M("position")->select();
        $group = M('auth_group')->select();
        $this->position = $position;
        $this->group = $group;
        $uid = I('id');
        $user = D("user")->where('id=%d',$uid)->find();
        // var_dump($user);die;
       $group = M('auth_group_access')->where('uid=%d',$uid)->find();
        // var_dump($group);die;
        $this->id = $user['id'];
        $this->emp_id = $user['emp_id'];
        $this->username=$user['username'];
        $this->email = $user['email'];
        $this->tel = $user['tel'];
        $this->img = __ROOT__.$user['img'];
        $this->age = $user['age'];
        $this->position_id = $user['position_id'];
        $this->group_id = $group['group_id'];
        

        if ($user['sex'] == 0) {
           $this->sex = "男";
        }else{
            $this->sex = "女";
        }
        $this->display();
    }

    // 编辑操作
    public function editHandle(){
        // var_dump(I());
        if (!IS_POST)$this->error('请求异常！');
        $user = D('user');
        $group = M('auth_group_access');
        // var_dump($group);die;
            $data['tel'] = I('tel');
            $data['position_id'] = I('position_id');
            $data['age'] = I('age');
          // var_dump($data);die;
        if ($user->where('id=%d',I('id'))->save($data)) {
                if ($group->where('uid=%d',I('id'))->save(array('group_id'=>I('group_id')))) {
                    $this->success("修改成功",__MODULE__."/User/");
                }else{
                    $this->error("所属组未改变！");
                }
            }else{
                $this->error("数据未改变！");
        }


    }


    // 锁定用户
    public function lock(){
        $user = M('user');
        $data = $user->where('id=%d',I('id'))->find();
        if ($data['status'] == 0) {
            $data['status'] = 1;
        }else{
            $data['status'] = 0;
        }
        // var_dump($data);die;
        $group = M('auth_group_access')->where('uid=%d',I('id'))->find();
        if($group['group_id'] == 4)$this->error('超级管理员不能锁定！');
        if ($user->save($data)) {
            $this->success("修改状态成功",__MODULE__."/User/");
        }else{
            $this->error("修改失败！");
        }


    }


    // 修改密码
    public function pass(){
        $this->display();
    }

    // 修改密码操作
    public function passHandle(){
        if(!IS_POST)$this->error('异常请求');
        $uid = session('uid');
        $nuser= M('user');
        $user= M('user')->where('id=%d',$uid)->find();
        $password = md5(I('password'));
        if ($user['password'] != $password) {
            $this->error('原密码错误！');
        };
        if (I('passwd') == '') {
            $this->error('新密码不能为空！');
        };
        if(I('passwd') != I('repasswd')){
            $this->error('两次新密码不一致！');
        };
        $passwd['id'] = $uid;
        $passwd['password'] = md5(I('passwd'));
        if ($nuser->save($passwd)) {
            $this->success("密码修改成功！");
        }else{
            $this->error('密码修改失败！');
        }

    }

    // 上传照片
    public function uploadImg(){
        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize   =     3145728 ;// 设置附件上传大小
        $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->subName  = array('date','Ym');
        $upload->rootPath =      './';
        $upload->savePath  =      '/upload/image/'; // 设置附件上传目录
        $info   =   $upload->upload();
        if(!$info) {// 上传错误提示错误信息
            $data['status']=0;
            $data['info']=$upload->getError();
            $data['path']=$info[0]['savepath'].$info[0]['savename'];
            $this->ajaxReturn($data);
        }else{// 上传成功
            
            $data['status']=1;
            $data['info']="上传成功";
            $data['path'] = $info[0]['savepath'].$info[0]['savename'];
            $this->ajaxReturn($data);
        }
    }

}