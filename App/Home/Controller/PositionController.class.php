<?php
namespace Home\Controller;
use Think\Controller;
class PositionController extends CommonController {
    public function index(){
        $data = D("position")->getPositions(8,'id asc');
        $this->position = $data['position']; 
        $this->page    = $data['page']; 
        $this->display();
    }

// 添加职位
    public function add(){

    	$this->display();

    }
// 添加职位操作
    public function addHandle(){
		if(!IS_POST)$this->error("请求方式错误");
		$position = D('position');
		if(!$position->create()){
			$this->error($position->getError());
		}else{
			if($position->add()){
					$this->success('添加成功',__MODULE__."/Position/");				
				}else{
					$this->error('添加失败');
			}
			
		}
    }

    // 删除职位 重要操作 暂时不显现
    public function delete(){
    	// var_dump(I());die;
        $user= M('user')->where('position_id=%d',I('id'))->select();
        if ($user)$this->error('该职位被占用，不能删除！');
    	$position = D('position');
    	if ($position->delete(I('id'))) {
    		$this->success('删除成功',__MODULE__."/Position/");
    	}else{
    		$this->error('删除失败');
    	}

    }

}
