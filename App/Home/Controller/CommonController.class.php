<?php
/**
 * Created by PhpStorm.
 * User: Shen
 * Date: 2015/3/31
 * Time: 22:01
 */
namespace Home\Controller;
use Think\Controller;
class CommonController extends Controller {
    public function _initialize(){
		//判断用户是否登陆
		if(!isset($_SESSION['username']) ) {
			redirect(__APP__.'/Home/Login');
		}
        //权限验证
        if(!authCheck(MODULE_NAME."/".CONTROLLER_NAME."/".ACTION_NAME,session('uid'))){
            // session(null);
            $this->error('你没有权限!');
        }  		
	}

}