<?php
/**
 * Created by PhpStorm.
 * User: Shen
 * Date: 2015/4/7
 * Time: 20:24
 */
namespace Home\Model;
use Think\Model\RelationModel;
class UserModel extends  RelationModel{

    protected $_link = array(

        'user' => array(
            'mapping_type' => self::BELONGS_TO,
            'class_name'   => 'position',
            'foreign_key'  => 'position_id',
            'mapping_fields'=>'name',
            'as_fields'   =>  'name:name'
        )


    );

// 自动验证
    protected $_validate = array(
         array('emp_id','require','员工ID必须！'), //默认情况下用正则进行验证
         array('emp_id','','员工ID已经存在！',0,'unique',1),
        // array('verify','require','验证码必须！'), //默认情况下用正则进行验证
         array('username','require','用户名必须！'), //默认情况下用正则进行验证
         array('username','','用户名已经存在！',0,'unique',1),
         array('tel','require','电话号码必须！'), //默认情况下用正则进行验证
         array('email','require','邮箱必须！'), //默认情况下用正则进行验证
         array('email','email','email格式错误'),  
         array('password','require','密码呢？！'), //默认情况下用正则进行验证
         array('repassword','password','确认密码不正确',0,'confirm'), // 验证确认密码是否和密码一致
        array('group_id','require','所属组必须！'),

    );
    // 自动添加属性
    protected $_auto = array ( 
         array('password','md5',3,'function') ,
         array('login_time','time',3,'function'), 
         array('status','0'),       
    );

//    获取用户
    public function getUsers($number,$where,$order){

        $count = $this->where($where)->count();
        $Page = new \Library\Page($count,$number);
        $Page ->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% <li>%HEADER%</li>');
        $data['page'] = $Page ->show();
        $data['user'] = $this ->where($where)
            ->order($order)
            ->limit($Page->firstRow.','.$Page->listRows)
            ->relation(true)
            ->select();
        return $data;

    }

}