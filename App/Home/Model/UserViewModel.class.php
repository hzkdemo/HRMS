<?php
/**
 * Created by PhpStorm.
 * User: Shen
 * Date: 2015/4/7
 * Time: 20:30
 */
namespace Home\Model;
use Think\Model\ViewModel;
class UserViewModel extends ViewModel{

    public $viewFields = array(
        'user'=>array('id','emp_id','username','position_id','email','tel','login_time','status'),
        'position' =>array('name'=>'position_name','_on'=>'user.position_id=position.id'),
        'auth_group_access'=>array('uid,group_id','_on'=>'auth_group_access.uid=user.id'),
        'auth_group'=>array('title','_on'=>'auth_group_access.group_id=auth_group.id'),
    );


    //输出所有用户
    public function getUsers($number,$order){
        $count = $this->count();
        $Page  = new \Library\Page($count,$number);// $number为分页数
        $Page->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% <li>%HEADER%</li>');
        $data['page'] = $Page->show();// 分页显示输出
        $data['users'] =
            $this->order($order)
                ->limit($Page->firstRow.','.$Page->listRows)
                ->select();
        return $data;
    }


}