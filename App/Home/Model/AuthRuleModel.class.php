<?php
/**
 * Created by PhpStorm.
 * User: Shen
 * Date: 2015/4/7
 * Time: 20:24
 */
namespace Home\Model;
use Think\Model\RelationModel;
class AuthRuleModel extends  RelationModel{
    protected $_validate = array(     
            array('name','require','规则标示必须！'),
            array('title','require','规则简介必须！')
        );
}