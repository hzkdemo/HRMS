<?php 
namespace Home\Model;
use Think\Model\ViewModel;
class RuleViewModel extends ViewModel{
	public $viewFields = array(
		'rule'=>array('_table'=>'__AUTH_RULE__','id','name','title','type','condition'=>'term','status','module_id'),
		'module'=>array('_table'=>'__AUTH_MODULE__','module_name','_on'=>'rule.module_id=module.id')
	);
}

 ?>