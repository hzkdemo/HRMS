<?php
/**
 * Created by PhpStorm.
 * User: Shen
 * Date: 2015/3/24
 * Time: 20:47
 */

return array(
    //数据库配置信息
    'DB_TYPE'    => 'mysql',        //数据库类型
    'DB_HOST'    => '127.0.0.1',    //主机地址
    'DB_NAME'    => 'hrms',         //数据库名称
    'DB_USER'    => 'root',         //用户名
    'DB_PWD'     => '',             //用户密码
    'DB_PORT'    => '3306',           //数据库端口
    'DB_PREFIX'  => 'think_',       //数据表前缀
    'DB_CHARSET' => 'utf8'         //数据库字符集



);