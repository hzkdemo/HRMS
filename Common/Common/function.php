<?php 
/**
 * authCheck 验证用户是否具有权限
 * @param  String  $rule     验证规则
 * @param  String  $uid      当前用户uid
 * @param  integer $type     如果type为1， condition字段就可以定义规则表达式
 * @param  string  $mode     验证方式
 * @param  string  $relation $relation为and时表示，用户需要同时具有规则1和规则2的权限。 $relation为or时，表示用户值需要具备其中一个条件即可。默认为or
 * @return boolean           如果用户有权限，返回true，反之则返回false
 */
	function authCheck($rule,$uid,$type=1, $mode='url', $relation='or'){
		//超级管理员跳过验证
		$auth=new \Library\Auth();
		//获取当前uid所在的角色组id
		$groups=$auth->getGroups($uid);
		// var_dump($groups);
		// var_dump($rule);die;
		if(in_array($groups[0]['id'], C('ADMINISTRATOR'))){
			return true;
		}else{			
			return $auth->check($rule,$uid,$type,$mode,$relation)?true:false;
		}
	}
/**
 * p 打印
 * @param  任意值  $data 传入的参数
 * @return 无返回值       返回值
 */
	function p($data){
		echo "<pre>";
		print_r($data);
	}